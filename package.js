Package.describe({
  name: 'mailgun',
  version: '0.1.1',
  summary: 'Meteor wrapper for mailgun',
  git: 'https://bitbucket.org/aspirinchaos/mailgun.git',
  documentation: 'README.md',
});

Npm.depends({
  'mailgun-js': '0.20.0',
});

Package.onUse((api) => {
  api.versionsFrom('1.5');
  api.use(['ecmascript', 'email']);
  api.mainModule('mailgun.js', 'server');
});

Package.onTest((api) => {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('mailgun');
  api.mainModule('mailgun-tests.js');
});
