// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from 'meteor/tinytest';

// Import and rename a variable exported by mailgun.js.
import { name as packageName } from 'meteor/mailgun';

// Write your tests here!
// Here is an example.
Tinytest.add('mailgun - example', function (test) {
  test.equal(packageName, 'mailgun');
});
