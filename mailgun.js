import mgjs from 'mailgun-js';
import { Email } from 'meteor/email';
import { Meteor } from 'meteor/meteor';

/**
 * Обертка для работы с mailgun
 */
class Mailgun {
  /**
   * Инстанс npm mailgun'а
   * @type {mailgun-js}
   * @private
   */
  _mailgun = null;

  /**
   * Отправитель
   * @type {string}
   * @private
   */
  _from = '';

  /**
   * Старая отправка через smtp
   * @type {function}
   * @private
   */
  _smtpSend = null;

  /**
   * Установка настроек для mailgun
   * @param apiKey
   * @param domain
   * @param from
   */
  setSettings({ apiKey, domain, from }) {
    this._mailgun = mgjs({ apiKey, domain });
    this._from = from;

    this._smtpSend = Email.send;
    /**
     * Переопределим функцию для отправки email
     */
    Email.send = options => this.send(options)
      .then((e, body) => new Promise((resolve, reject) => {
        if (e) {
          console.log(e);
          try {
          // send throw smtp
            this._smtpSend(options);
          } catch (err) {
            reject(err);
          }
        }
        resolve(body);
      }));
  }

  /**
   * Отправка email
   * @param from {string} от кого [optional]
   * @param to {string} кому
   * @param subject {string} тема
   * @param text {string} текст
   * @param html {string} html письмо
   * @returns {Promise<any>}
   */
  send({
    from, to, subject, text, html,
  }) {
    return new Promise((resolve, reject) => {
      if (!this._mailgun) {
        reject(Meteor.Error('settings-not-set', 'Не установлены настройки для Mailgun!'));
      }
      this._mailgun.messages().send({
        from: from || this._from, to, subject, text, html,
      }, (error, body) => {
        // для правильной обработки smtp отправки
        resolve(error, body);
      });
    });
  }
}

const MG = new Mailgun();

export { MG as Mailgun };
